﻿using GeneralModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WebAppRequester
{
    class Program
    {
        static HttpClient client = new HttpClient();

        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            // New code:
            client.BaseAddress = new Uri("http://localhost:55268/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            Filter filter = new Filter()
                .AddStatuses("Testing", "Working")
                .AddIds(11, 12, 13, 14)
                .AddDates(
                    new Tuple<DateTime, DateTime>(new DateTime(2017, 02, 13), new DateTime(2017, 02, 13)),
                    new Tuple<DateTime, DateTime>(new DateTime(2017, 06, 13), new DateTime(2017, 07, 13))
                );

            var a = GetProductAsync("http://localhost:49159/api/workplans", filter);
            a.Wait();

            foreach (var i in a.Result)
            {
                Console.WriteLine(i.Id);
            }

            Console.ReadLine();
        }

        static async Task<List<Workplan>> GetProductAsync(string path, Filter filter)
        {
            List<Workplan> product = null;
            HttpResponseMessage response = await client.PostAsJsonAsync<Filter>(path, filter);
            if (response.IsSuccessStatusCode)
            {
                product = await response.Content.ReadAsAsync<List<Workplan>>();
            }
            return product;
        }
    }
}
