﻿using GeneralModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp01.Helpers
{
    public class TreeHelper
    {
        public static MvcHtmlString Tree(
            IEnumerable<Workplan> list,
            string template = "{name}")
        {
            string result = "";
            List<object> nodes = new List<object>();
            foreach (var item in list)
            {
                if (item.ParrentId == null)
                {
                    result += $"<li> { TreeNode(item, list, template) } </li>";
                }
            }
            if (string.IsNullOrWhiteSpace(result))
                return new MvcHtmlString("");
            return new MvcHtmlString($@"<ul>{Environment.NewLine + result + Environment.NewLine}</ul>");
        }

        private static string TreeNode(
            Workplan workplan,
            IEnumerable<Workplan>
            list, string template)
        {
            string result = "";
            var childs = list.Where(a => a.ParrentId == workplan.Id);
            string element = TreeLeaflet(workplan, template);
            if (childs.Count() == 0)
                return element;
            foreach (var item in childs)
            {
                result += "<li>"
                        + Environment.NewLine +
                        TreeNode(item, list, template)
                        + Environment.NewLine +
                        "</li>";
            }
            return $@"
                    <details>
                        <summary>
                            {element}
                        </summary>
                        <ul>
                            {result}
                        </ul>
                    </details>";
        }
        private static string TreeLeaflet(
            Workplan wp,
            string template)
        {
            string result = template;
            result = result.Replace("{name}", wp.Name);
            result = result.Replace("{status}", wp.Status.ToString());
            result = result.Replace(
                "{date}",
                wp.StartDate.HasValue ?
                    " " + wp.StartDate.Value.ToString() :
                    "");

            return result;
        }
    }
}