﻿using GeneralModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApp01.Models;

namespace WebApp01.Controllers.api
{
    public class ProjectsController : ApiController
    {
        [HttpGet]
        public List<Project> GetProject([FromUri] int id)
        {
            var conn = ConfigurationManager.ConnectionStrings["Entities"].ConnectionString;
            using (var a = new SqlConnection(conn))
            {
                a.Open();

                //Creating command
                SqlCommand command = new SqlCommand($"SELECT * FROM GetWorkstream({id})", a);
                command.CommandType = CommandType.Text;

                //execute
                SqlDataReader reader = command.ExecuteReader();

                List<Project> prs = new List<Project>();

                while (reader.Read())
                {
                    int _id = reader.GetInt32(0);
                    int streamid = reader.GetInt32(1);
                    string name = reader[2].ToString().TrimEnd();
                    string status = reader[3].ToString().TrimEnd();
                    int? order = null;
                    if (!reader.IsDBNull(3))
                        order = reader.GetInt32(3);

                    Project project = new Project()
                    {
                        Id = _id,
                        WorkstreamId = streamid,
                        Status = status,
                        Name = name,
                        Order = order
                    };

                    prs.Add(project);
                }
                return prs;
            }
        }
    }
}
