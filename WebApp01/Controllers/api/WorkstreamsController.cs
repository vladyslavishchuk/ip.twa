﻿using GeneralModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApp01.Controllers.api
{
    public class WorkstreamsController : ApiController
    {
        [HttpGet]
        public List<Workstream> GetWorkstreams()
        {
            var conn = ConfigurationManager.ConnectionStrings["Entities"].ConnectionString;
            using (var a = new SqlConnection(conn))
            {
                a.Open();

                //Creating command
                SqlCommand command = new SqlCommand("SELECT * FROM AllWorkstreams()", a);
                command.CommandType = CommandType.Text;

                //execute
                SqlDataReader reader = command.ExecuteReader();

                List<Workstream> wss = new List<Workstream>();

                while (reader.Read())
                {
                    int _id = reader.GetInt32(0);
                    string name = reader[1].ToString().TrimEnd();
                    string status = reader[2].ToString().TrimEnd();
                    int? order = null;
                    if (!reader.IsDBNull(3))
                        order = reader.GetInt32(3);

                    Workstream workstream = new Workstream()
                    {
                        Id = _id,
                        Status = status,
                        Name = name,
                        Order = order
                    };

                    wss.Add(workstream);
                }
                return wss;
            }
        }
    }
}
