﻿using GeneralModels;
using Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;

namespace WebApp01.Controllers
{
    public class WorkplansController : ApiController
    {
        [HttpPost]
        public List<Workplan> GetWorkplan([FromBody] Filter filter)
        {
            var conn = ConfigurationManager.ConnectionStrings["Entities"].ConnectionString;
            using (var a = new SqlConnection(conn))
            {
                a.Open();

                //statuses
                DataTable statuses = new DataTable();
                statuses.Columns.Add("Item", typeof(string));

                foreach (Status? i in filter.Statuses)
                {
                    if (i != null)
                        statuses.Rows.Add(i.Value.ToString());
                }

                //Names
                DataTable names = new DataTable();
                names.Columns.Add("Item", typeof(string));

                foreach (string i in filter.Names)
                {
                    names.Rows.Add(i);
                }

                //IDs
                DataTable ids = new DataTable();
                ids.Columns.Add("Item", typeof(int));

                foreach (int i in filter.Ids)
                {
                    ids.Rows.Add(i);
                }

                //Dates
                DataTable dates = new DataTable();
                dates.Columns.Add("start", typeof(DateTime));
                dates.Columns.Add("end", typeof(DateTime));

                foreach (var i in filter.Dates)
                {
                    dates.Rows.Add(i.Item1, i.Item2);
                }

                //Creating command
                SqlCommand command = new SqlCommand("ByFilter", a);
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter statusParam = command.Parameters.Add("@Statuses", SqlDbType.Structured);
                statusParam.Value = statuses;
                statusParam.TypeName = "[dbo].[StringArray]";

                SqlParameter IdsParam = command.Parameters.Add("@Ids", SqlDbType.Structured);
                IdsParam.Value = ids;
                IdsParam.TypeName = "[dbo].[IntList]";

                SqlParameter DatesParam = command.Parameters.Add("@Dates", SqlDbType.Structured);
                DatesParam.Value = dates;
                DatesParam.TypeName = "[dbo].[DateTimePairList]";

                SqlParameter nameParam = command.Parameters.Add("@Names", SqlDbType.Structured);
                nameParam.Value = names;
                nameParam.TypeName = "[dbo].[StringArray]";

                SqlParameter ProjectParam = command.Parameters.Add("@ProjectFilter", SqlDbType.Int);
                ProjectParam.Value = (object)filter.ProjectID ?? DBNull.Value;

                //execute
                SqlDataReader reader = command.ExecuteReader();

                List<Workplan> wps = new List<Workplan>();

                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    Status status = (Status)Enum.Parse(typeof(Status), reader.GetString(1).TrimEnd());
                    string name = reader.GetString(2).TrimEnd();
                    DateTime? date = null;
                    if (!reader.IsDBNull(3))
                        date = reader.GetDateTime(3);
                    int pid = reader.GetInt32(4);
                    string order = reader.IsDBNull(6) ? null : reader.GetString(6);
                    int? parrent = reader.IsDBNull(7) ? null : (int?)reader.GetInt32(7);

                    Workplan workplan = new Workplan()
                    {
                        Id = id,
                        Status = status,
                        Name = name,
                        StartDate = date,
                        Order = order,
                        ParrentId = parrent,
                    };

                    wps.Add(workplan);
                }
                return wps;
            }
        }
        //// GET: api/Workplans/5
        //[ResponseType(typeof(Workplan))]
        //public IHttpActionResult GetWorkplan(int id)
        //{
        //    Workplan workplan = null; //db.Workplan.Find(id);
        //    if (workplan == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(workplan);
        //}
    }
}