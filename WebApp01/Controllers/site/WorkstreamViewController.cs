﻿using GeneralModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace WebApp01.Controllers.site
{
    public class WorkstreamViewController : Controller
    {
        private const string WorkstreamsAPIUri = "http://localhost:49159/api/Workstreams";
        // GET: Workstreams
        public ActionResult Index()
        {
            List<Workstream> workstreams = new List<Workstream>();

            HttpResponseMessage response = new HttpClient().GetAsync(WorkstreamsAPIUri)
                .GetAwaiter().GetResult();
            if (response.IsSuccessStatusCode)
            {
                workstreams = response.Content.ReadAsAsync<List<Workstream>>().GetAwaiter().GetResult();
            }

            return View(workstreams);
        }
    }
}