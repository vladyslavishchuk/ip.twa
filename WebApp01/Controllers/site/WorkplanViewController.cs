﻿using GeneralModels;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using WebApp01.Models;

namespace WebApp01.Controllers.site
{
    public class WorkplanViewController : Controller
    {
        private const string WorkplansAPIUri = "http://localhost:49159/api/workplans";

        // GET: WorkplanView
        public ActionResult Index(int ProjectFilter)
        {
            Status? a = null;
            if (!string.IsNullOrWhiteSpace(Request.Form["StatusFilter"]))
            {
                a = (Status)Enum.Parse(typeof(Status), Request.Form["StatusFilter"]);
            }
            string b = Request.Form["IdFilter"];
            string c = Request.Form["DateFilter"];
            string d = Request.Form["NameFilter"];

            GeneralModels.Filter filter = GetFilter(ref a, ref b, ref c, ref d);
            filter.ProjectID = ProjectFilter;

            List<Workplan> wp = new List<Workplan>();

            HttpResponseMessage response = new HttpClient()
                .PostAsJsonAsync<GeneralModels.Filter>(WorkplansAPIUri, filter)
                .GetAwaiter().GetResult();
            if (response.IsSuccessStatusCode)
            {
                wp = response.Content.ReadAsAsync<List<Workplan>>().GetAwaiter().GetResult();
            }

            WPViewModel model =
            new WPViewModel
            {
                StatusFilter = a,
                IdFilter = b,
                DateFilter = c,
                NamesFilter = d,
                Values = wp,
            };

            return View(model);
        }
        private GeneralModels.Filter GetFilter(
            ref Status? statuses,
            ref string ids,
            ref string dates,
            ref string names)
        {
            GeneralModels.Filter result = new GeneralModels.Filter();

            try
            {
                if(statuses != null)
                result.AddStatuses(statuses);
            }
            catch
            {
                statuses = null;
            }

            try
            {
                result.AddNames(
                    names.Split(',')
                    .Select(a => a.Trim())
                    .Where(a => !string.IsNullOrEmpty(a))
                    .ToArray()
                    );
            }
            catch
            {
                names = null;
            }

            try
            {
                result.AddIds(ids.Split(',').Select(a => int.Parse(a)).ToArray());
            }
            catch
            {
                ids = null;
            }

            try
            {
                result.AddDates(
                    dates.Split(',') //Split date pairs by ','
                    .Select(a => a.Split('-') //Split pairs by '-'
                        .Select(b => Convert.ToDateTime(b))) //Convert string to date
                                                             //Convert IEnumerable to Tuple
                    .Select(a => new Tuple<DateTime, DateTime>(a.ElementAt(0), a.ElementAt(1)))
                    .ToArray());
            }
            catch
            {
                dates = null;
            }
            return result;
        }
    }
}