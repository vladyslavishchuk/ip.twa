﻿using GeneralModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace WebApp01.Controllers.site
{
    public class ProjectViewController : Controller
    {
        private const string ProjectsAPIUri = "http://localhost:49159/api/projects";
        // GET: ProjectView
        public ActionResult Index(int stream)
        {
            List<Project> projects = new List<Project>();

            HttpResponseMessage response = new HttpClient().GetAsync(ProjectsAPIUri + $"/{stream}")
                .GetAwaiter().GetResult();
            if (response.IsSuccessStatusCode)
            {
                projects = response.Content.ReadAsAsync<List<Project>>().GetAwaiter().GetResult();
            }

            return View(projects);
        }
    }
}