﻿using GeneralModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace WebApp01.Controllers.site
{
    public class WorkplanTreeController : Controller
    {
        private const string WorkplansAPIUri = "http://localhost:49159/api/workplans";
        // GET: WorkplanTree
        public ActionResult Index(int ProjectFilter)
        {
            List<Workplan> wp = new List<Workplan>();
            var filter = new GeneralModels.Filter() { ProjectID = ProjectFilter };

            HttpResponseMessage response = new HttpClient()
                .PostAsJsonAsync<GeneralModels.Filter>(WorkplansAPIUri, filter)
                .GetAwaiter().GetResult();
            if (response.IsSuccessStatusCode)
            {
                wp = response.Content.ReadAsAsync<List<Workplan>>().GetAwaiter().GetResult();
            }

            return View(wp);
        }
    }
}