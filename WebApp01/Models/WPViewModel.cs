﻿using GeneralModels;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp01.Models
{
    public class WPViewModel
    {
        public Status? StatusFilter;
        public string NamesFilter;
        public string IdFilter;
        public string DateFilter;
        public IEnumerable<Workplan> Values;
    }
}