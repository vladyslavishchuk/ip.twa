﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GeneralModels
{
    [DataContract]
    public class Workstream
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public Nullable<int> Order { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Status { get; set; }
    }
}
