﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace GeneralModels
{
    [DataContract]
    public class Workplan
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public Nullable<int> ProjectId { get; set; }
        [DataMember]
        public Nullable<int> ParrentId { get; set; }
        [DataMember]
        public string Order { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public Status Status { get; set; }
        [DataMember]
        public Nullable<System.DateTime> StartDate { get; set; }
        [DataMember]
        public Nullable<System.DateTime> EndDate { get; set; }
    }
}