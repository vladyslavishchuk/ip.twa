﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeneralModels
{
    public class Filter
    {
        public List<Status?> Statuses { get; set; } = new List<Status?>();
        public List<string> Names { get; set; } = new List<string>();
        public List<Tuple<DateTime, DateTime>> Dates { get; set; }
            = new List<Tuple<DateTime, DateTime>>();
        public List<int> Ids { get; set; } = new List<int>();
        public int? ProjectID { get; set; } = null;

        public Filter SetProjectID(int project)
        {
            ProjectID = project;
            return this;
        }

        public Filter AddStatuses(params Status?[] statuses)
        {
            Statuses.AddRange(statuses);
            return this;
        }

        public Filter AddNames(params string[] names)
        {
            Names.AddRange(names);
            return this;
        }

        public Filter AddIds(params int[] ids)
        {
            Ids.AddRange(ids);
            return this;
        }

        public Filter AddDates(params Tuple<DateTime, DateTime>[] dates)
        {
            Dates.AddRange(dates);
            return this;
        }

        public Filter AddDates(params DateTime[] dates)
        {
            if (dates.Length % 2 == 1)
                throw new ArgumentException("dates must be group by 2!");
            var dds = new List<Tuple<DateTime, DateTime>>();
            for (int i = 0; i < dates.Length / 2; i++)
            {
                dds.Add(
                    new Tuple<DateTime, DateTime>(
                        dates[i * 2], 
                        dates[i * 2 + 1])
                        );
            }
            AddDates(dds.ToArray());
            return this;
        }
    }
}